#include <iostream>
#include <algorithm>
#ifndef tree_hpp
#define tree_hpp

template <class T>
struct node
{
    T value;

    // balance factor
    // lewe poddrzewo wieksze - ujemny
    // prawe - dodatni
    int bf;
    node* up;
    node* left;
    node* right;
    node(T v)
    {
        value = v;
        bf = 0;
        up = NULL;
        left = NULL;
        right = NULL;
    };
    node()
    {
        bf = 0;
        up = NULL;
        left = NULL;
        right = NULL;
    }

    node* Successor()
    {
        node* p = this;
        if(right)
        {
            p = right;
            while (p->left)
            {
                p = p->left;
            }
            return p;
        }

        node* r = up;


        while( r && (p == r->right))
        {
            p = r;
            r = r->up;
        }
        return r;
    }

    node* Predecessor()
    {
        if(left) return left;
        node* r = up;
        node* p = this;
        while( r && (p == r->left))
        {
            p = r;
            r = r->up;
        }
        return r;
    }
    bool operator== (const node &n)
    {
        return this.value == n.value;
    };
    bool operator< (const node &n)
    {
        return this->value < n.value;
    };
    bool operator> (const node &n)
    {
        return this->value > n.value;
    };
    bool operator<= (const node &n)
    {
        return !(*this>n);
    };
    bool operator>= (const node &n)
    {
        return !(*this<n);
    };
};

template <class T>
class tree
{
private:
    bool isempty;
    int length;
    node<T>* root;
    node<T>* first;
    node<T>* last;

    inline void LengthAdd()
    {
        length++;
        isempty = false;
    }

    inline void LengthSubdivide()
    {
        if(length) length--;
        if(!length) isempty = true;
    }

    void Destroy( node<T>* n)
    {
        if(n->right) Destroy(n->right);
        if(n->left) Destroy(n->left);
        delete n;
    }

    node<T>* Find(T val)
    {
        node<T>* n = root;
        while(n)
        {
            if(n->value == val)
            {
                return n;
            }
            else if(n->value < val)
            {
                n = n->right;
            }
            else if(n->value > val)
            {
                n = n->left;
            }
        }
        return NULL;
    }

    bool Identity(node<T>* n1, node<T>* n2)
    {
        if(n1->value != n2->value) return false;
        if(!n1->left && n2->left) return false;
        if(n1->left && !n2->left) return false;
        if(!n1->right && n2->right ) return false;
        if(n1->right && !n2->right ) return false;

        bool sw1 = 1, sw2 = 1;
        if(n1->left)
        {
            sw1 = Identity(n1->left, n2->left);
        }
        if(n1->right)
        {
            sw2 = Identity(n1->right, n2->right);
        }

        return (sw1 & sw2);
    }

    int GetHeight(node<T> *n)
    {
        if(!n)return 0;
        if(!n->left && !n->right)
        {
            n->bf = 0;
            return 1;
        }
        else
        {
            int hl = GetHeight(n->left);
            int hr = GetHeight(n->right);
            n->bf = hr - hl;
            int result = std::max( hl, hr ) + 1;
            return ( result );
        }
    }

    void repairBF(node <T>* n)
    {
        if(!n) return;
        int BF = n->bf;
        if(n->left) repairBF(n->left);
        if(n->right) repairBF(n->right);


        if( BF == 2 )
        {

            if(n->right->bf != -1)
            {
                LeftRotate(n);
            }
            else
            {
                RLrotate(n);
            }
        }
        else if (BF == -2)
        {

            if(n->left->bf != 1)
            {
                RightRotate(n);
            }
            else
            {
                LRrotate(n);
            }
        }
    }

public:


    node<T>* GetRoot() {return root;}
    node<T>* GetFirst() {return first;}
    node<T>* GetLast() {return last;}


    tree()
    {
        isempty = true;
        length = 0;
        root = NULL;
        first = NULL;
        last = NULL;
    };

    // stala wartosc - const!
    inline int GetLength() const
    {
        return length;
    }

    bool Contains(const T& val)
    {
        if(Find(val) ) return true;
        return false;
    }

    void Add(T val)
    {
        node<T>* n = new node<T>;
        n->value = val;
        if(!root)
        {
            root = n;
            first = n;
            last = n;
            LengthAdd();
            return;
        }
        node<T>* wsk = root;
        while(wsk)
        {
            if(*n<*wsk)
            {
                if(!wsk->left)
                {
                    if(first == wsk) first = n;
                    wsk->left = n;
                    wsk->bf--;

                    n->up = wsk;
                    LengthAdd();

                    if(!wsk->right)
                    {
                        GetHeight(root);
                    }
                    wsk = n->up->up;
                    if(wsk) { repairBF(wsk);}
                    return;
                }
                else
                {
                    wsk = wsk->left;
                }
            }
            else
            {
                if(!wsk->right)
                {
                    if(last == wsk) last = n;
                    wsk->right = n;
                    wsk->bf++;
                    n->up = wsk;
                    LengthAdd();

                    if(!wsk->left)
                    {
                        GetHeight(root);
                    }
                    wsk = n->up->up;
                    if(wsk) {repairBF(wsk);}
                    return;
                }
                else
                {
                    wsk = wsk->right;
                }
            }
        }

    }

    void Remove(T val)
    {
        node<T>* n1 = Find(val);
        node<T>* n2 = NULL; // wskazania pomocnicze
        node<T>* n3 = NULL;
        if(n1) // jezeli wartosc w ogole jest w drzewie
        {
            if(!n1->left || !n1->right)
            {
                n2 = n1;
                if(first == n1) first = n1->Successor();
                if(last == n1) last = n1->Predecessor();
            }
            else
            {
                n2 = n1->Successor();
            }

            if(n2->left)
            {
                n3 = n2->left;
            }
            else
            {
                n3 = n2->right;
            }

            if(n3)
            {
                n3->up = n2->up;
            }

            if(root == n2)
            {
                root = n3;
            }
            else
            {
                if(n2 == n2->up->right)
                {
                    n2->up->right = n3;
                }
                else
                {
                    n2->up->left = n3;
                }
            }

            if(n1 != n2)
            {n1->value = n2->value;}

            LengthSubdivide();
            delete n2;

            GetHeight(root);
            repairBF(root);

        }

    }

    void Clear()
    {
        Destroy(root);
        length = 0;
        isempty = true;
        root = NULL;
        first = NULL;
        last = NULL;
    }

    void FixBF()
    {
        GetHeight(root);
    }

    node<T>* RightRotate(node<T> *x)
    {
        node<T> *y = x->left;
        node<T> *z = y->right;

        // perform rotation
        if(root == x) root = y;
        y->right = x;
        y->up = x->up;
        x->left = z;

        if(x->up)
        {
            if(x == x->up->right) {x->up->right = y;}
            else {x->up->left = y;}

        }

        x->up = y;
        if(z) z->up = x;
        node<T> *result = y;

        //fix balance factor
        if(y->bf == -1)
        {
            x->bf = 0;
            y->bf = 0;
            x = y->up;
            while (x)
            {
                if(y == x->left) {x->bf++;}
                else { x->bf--; };
                y = x;
                x = x->up;
            }
        }
        else if(y->bf == 0)
        {
            x->bf = -1;
            y->bf = 1;
        }
        else if(y->bf == 1)
        {
            x->bf = 1;
            y->bf = 2;
        }


        return result;
    }

    node<T>* LeftRotate(node<T> *x)
    {
        node<T> *y = x->right;
        node<T> *z = y->left;



        //perform rotation
        if(root == x) {root = y;}
        y->left = x;
        y->up = x->up;
        x->right = z;

        if(x->up)
        {
            if(x == x->up->right) {x->up->right = y;}
            else {x->up->left = y;}

        }

        x->up = y;
        if(z) z->up = x;


        node<T> *result = y;


        if(y->bf == -1)
        {
            x->bf = 1;
            y->bf = -2;
        }
        else if(y->bf == 0)
        {
            x->bf = 1;
            y->bf = -1;
        }
        else if(y->bf == 1)
        {
            x->bf = 0;
            y->bf = 0;

            // naprawiamy bf na gorze
            x = y->up;
            while (x)
            {
                if(y == x->left) {x->bf++;}
                else { x->bf--; };
                y = x;
                x = x->up;
            }

        }

        return result;
    }

    node<T>* RLrotate(node<T>* x)
    {
        node<T>* y = x->right;
        node<T>* z = y->left;
        node<T>* w = z->right;

        /*y->right = x;
        y->up = x->up;
        x->left = z;
        x->up = y;
        if(z) z->up = x;*/

        // R rotacja na dole
        z->right = y;
        z->up = y->up; // x
        y->left = w;

        if(y->up)
        {
            if(y == y->up->right) {y->up->right = z;}
            else {y->up->left = z;}

        }

        y->up = z;
        if(w) w->up = y;

        // L rotacja na gorze
        node<T>* w2 = z->left;
        if(root == x) root = z;
        z->left = x;
        z->up = x->up;
        x->right = w2;

        if(x->up)
        {
            if(x == x->up->right) {x->up->right = z;}
            else {x->up->left = z;}

        }

        x->up = z;
        if(w2) w2->up = x;

        if(z->bf == -1)
        {
            x->bf = 0;
            y->bf = 1;
            z->bf = 0;
        }
        else if(z->bf == 0)
        {
            x->bf = 0;
            y->bf = 0;
            z->bf = 0;
        }
        else if(z->bf == 1)
        {
            x->bf = -1;
            y->bf = 0;
            z->bf = 0;
        }
        // naprawiamy bf powyzej
        x = z->up;
        y = z;
        while(x)
        {
            if(y == x->left) {x->bf++;}
            else { x->bf--; };
            y = x;
            x = x->up;
        }
        return z;
    }

    node<T>* LRrotate( node<T>* x)
    {
        node<T>* y = x->left;
        node<T>* z = y->right;
        node<T>* w = z->left;

        // L rotacja na dole
        z->left = y;
        z->up = y->up;
        y->right = w;

        if(y->up)
        {
            if(y == y->up->right) {y->up->right = z;}
            else {y->up->left = z;}

        }

        y->up = z;
        if(w) w->up = y;



        // R rotacja na gorze
        node<T>* w2 = z->right;
        if(root == x) root = z;
        z->right = x;
        z->up = x->up;
        x->left = w2;

        if(x->up)
        {
            if(x == x->up->right) {x->up->right = z;}
            else {x->up->left = z;}

        }

        x->up = z;
        if(w2) w2->up = x;

        if(z->bf == -1)
        {
            x->bf = 1;
            y->bf = 0;
            z->bf = 0;
        }
        else if(z->bf == 0)
        {
            x->bf = 0;
            y->bf = 0;
            z->bf = 0;
        }
        else if(z->bf == 1)
        {
            x->bf = 0;
            y->bf = -1;
            z->bf = 0;
        }

        // naprawiamy bf powyzej
        x = z->up;
        y = z;
        while(x)
        {
            if(y == x->left) {x->bf++;}
            else { x->bf--; };
            y = x;
            x = x->up;
        }
        return z;

    }

    class iter;

    void Out(int a, T s[], node<T>* nod, bool trigger = 1)
    {
        if(nod)
        {
            s[a] = nod->value;
        }
        if(nod->left)
        {
            Out(2*a, s, nod->left, 0);
        }
        else
        {
            s[2*a] = 0;
        }
        if(nod->right)
        {
            Out(2*a + 1, s, nod->right, 0);
        }
        else
        {
            s[2*a+1] = 0;
        }
        if(trigger)
        {
            int x = 1;
            while (x < length)
            {
                x = x*2;
            }
            x--;
            for (int i = 1; i <= x; i++)
            {
                std::cout<<s[i]<<" ";
            }
            std::cout<<std::endl;
        }

    }

    tree<T> operator+ (const tree<T> &t2)
    {
        // should create new tree
        iter it = iter(t2);
        int x = t2.GetLength();
        for(int i = 0; i < x; i++ )
        {
            if(it.current)
            {
                this->Add(it.current->value);
                ++it;
            }
        }
        return *this;
    }

    tree<T> operator- (const tree<T> &t2)
    {
        // should create new tree
        iter it = iter(t2);
        int n = t2.GetLength();
        for(int i = 0; i < n; i++ )
        {
            if(it.current)
            {
                this->Remove(it.current->value);
                ++it;
            }
        }
        return *this;
    }

    bool operator== (const tree<T> &t2)
    {
        return Identity(this->root, t2.root);
    }

    bool operator!= (const tree<T> &t2)
    {
        return !(*this == t2);
    }

    tree& operator= ( tree t2)
    {
        if(*this == t2) return *this;
        this->Clear();
        iter it = iter(t2);
        int n = t2.GetLength();
        for(int i = 0; i < n; i++ )
        {
            if(it.current)
            {
                this->Add(it.current->value);
                ++it;
            }
        }
        return *this;
    }

    void R()
    {
        repairBF(root);
    }
};

template <class T>
class tree<T>::iter
{
public:
    node<T>* current;
    iter(tree<T> tr)
    {
        current = tr.first;
    }
    void A()
    {
        std::cout<<current->value<<std::endl;
    };

    iter& operator= (const iter &v)
    {
        if(this == v) return *this;
        this.current = v.current;
        return *this;
    };

    bool operator== (const iter &v)
    {
        node<T>* n1 = this->current;
        node<T>* n2 = v.current;

        if(n1->value != n2->value) return false;
        if(!n1->left && n2->left) return false;
        if(n1->left && !n2->left) return false;
        if(!n1->right && n2->right ) return false;
        if(n1->right && !n2->right ) return false;

        return true;
    }

    iter& operator++()
    {
        this->current = this->current->Successor();
        return *this;
    }

    iter& operator--()
    {
        this->current = this->current->Predecessor();
        return *this;
    }
};

#endif // tree_hpp
