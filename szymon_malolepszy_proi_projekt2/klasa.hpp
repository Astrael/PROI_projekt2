#include <iostream>
#ifndef klasa_hpp
#define klasa_hpp

// wartosci parametru dla f-cji Find()
enum f_control
{
    v_prev, // element przed elementem o danej wetosci
    v_exact, // element o danej wartosci
    index // element o danym indeksie
};

enum a_control
{
    position,
    value,
    top,
    bottom,
};

// pojedynczy element kolejki
class element
{
public:
    int value;
    element* next;
    element (int v);
};

class queue
{
    bool isempty;
    int length;
    element* first;
    element* last;


    // f-cja zwraca wskaznik dla innych funkcji
    element* Find(f_control c, int v);

    element* Find_index(int v);

    element* Find_v_prev(int v);

    element* Find_v_exact(int v);

    // zmniejsza dlugosc i dba o pole empty
    inline void LengthSubdivide()
    {
        length = std::max(0, length-1);
        if(!length) isempty = true;
    }

    // zwieksza dlugosc i dba o pole empty
    inline void LengthAdd()
    {
        length++;
        isempty = false;
    }

    // sprawdza, czy element v sie powtarza
    bool Multiple(int v);

public:
    queue();

    ~queue();

    queue (const queue &q);

    int GetLength();

    bool IsEmpty();

    int GetValue(a_control c, int v = 0);

    int GetValue_top();

    int GetValue_bottom();

    int GetValue_index(int i);

    void Add(int v, a_control c = bottom, int i = 0);

    void Add_bottom(int v);

    void Add_top(int v);

    void Add_value(int v, int i);

    void Add_index(int v, int i);

    void Remove(int v = 0, a_control c = bottom);

    void Remove_top();

    void Remove_bottom();

    void Remove_index(int i);

    void Remove_value(int i);

    void RemoveDoubles();

    friend std::ostream& operator<<(std::ostream &os, const queue &q);

    int operator [](int i);

    queue operator +( const queue &q2);

    queue operator -( const queue &q2);

    bool operator== ( const queue &q2);

    bool operator< ( const queue &q2);

    bool operator> ( const queue &q2);

    bool operator<= ( const queue &q2);

    bool operator>= ( const queue &q2);

    queue& operator= (const queue  &q);

    queue operator+= (const queue &q);

    queue& operator-= (const queue &q);
};
#endif // klasa_hpp
