#include <iostream>
#include "tree.hpp"
#include "klasa.hpp"

using namespace std;

void test1()
{
    tree<int> t;


    t.Add(1);
    t.Add(2);
    t.Add(3);
    node<int> *n = t.GetRoot();
    if(n->value == 2 && n->left->value == 1 && n->right->value == 3 && t.GetLength() == 3 ) cout<<"Test1 complete!"<<endl;
}

void test2()
{
    tree<int> t1;
    tree<int> t2;
    t1.Add(1);
    t1.Add(2);
    t2.Add(3);
    t2.Add(4);
    t2.Add(5);
    t2.Add(6);
    if(t1 != t2 && !(t1 == t2)) cout<<"Test2 complete!"<<endl;
}
void test3()
{
    tree<int> t1;
    tree<int> t2;
    t1.Add(1);
    t1.Add(2);
    t2.Add(3);
    t2.Add(4);
    t2.Add(5);
    t1 = t1 + t2;
    tree<int>::iter a = tree<int>::iter(t1);
    int n = t1.GetLength();
    node<int> *nd = t1.GetRoot();
    int sw = 1;
    if(nd->value != 2) sw = 0;
    if(nd->left->value != 1) sw = 0;
    if(nd->right->value != 4) sw = 0;
    if(nd->right->left->value != 3) sw = 0;
    if(nd->right->right->value != 5) sw = 0;

    if(sw) cout<<"Test3 complete!"<<endl;
    //cout<<nd->right->left->value<<endl;

}
void test4()
{
    tree<int> t1;
    tree<int> t2;
    t1.Add(1);
    t1.Add(2);
    t1.Add(3);
    t2.Add(2);
    t1 = t1 - t2;
    int n = t1.GetLength();
    node<int> *nd = t1.GetRoot();
    int sw = 1;
    if(nd->value != 2) sw = 3;
    if(nd->left->value != 1) sw = 1;

    if(sw) cout<<"Test4 complete!"<<endl;
    //cout<<nd->right->left->value<<endl;

}

void test5()
{
    tree<queue> t;
    queue q;
    q.Add_bottom(1);
    t.Add(q);
    t.Add(q);
    t.Add(q);
    if(t.GetFirst()->value == q)cout<<"Test5 complete!"<<endl;
}





void test13()
{
    tree<int> t;
    t.Add(1);
    t.Add(2);
    t.Add(3);

    tree<int>::iter a = tree<int>::iter(t);
    while(a.current)
    {
        cout<<a.current->value<<" "<<a.current->bf<<endl;
        ++a;
    }
    a.current = t.GetFirst();
}
void test14(int a, int b, int c)
{
    tree<int> t;
    t.Add(a);
    t.Add(b);
    t.Add(c);

    tree<int>::iter it = tree<int>::iter(t);
    while(it.current)
    {
        cout<<it.current->value<<" "<<it.current->bf<<endl;
        ++it;
    }
    //t.R();
    /*cout<<"Root: "<<t.root->value<< " "<<t.root->bf<<endl;
    cout<<"Left: "<<t.root->left->value<< " "<<t.root->left->bf<<endl;
    cout<<"Right: "<<t.root->right->value<< " "<<t.root->right->bf<<endl;*/

}


void test15(int a, int b)
{
    tree<int> t;
    t.Add(a);
    t.Add(b);

    t.Remove(b);

    tree<int>::iter it = tree<int>::iter(t);
    /*cout<<"First: "<<t.first->value<<"Last: "<<t.last->value<<endl;*/

    while(it.current)
    {
        cout<<it.current->value<<" "<<it.current->bf<<endl;
        ++it;
    }

}

int main()
{
    cout << "Kill yourself" << endl;
    test1();
    test2();
    test3();
    test4();
    test5();

    test14(1,2,3);








    return 0;
}
