#include <iostream>
#include <algorithm>
#include "klasa.hpp"
#include <stdlib.h>

element::element(int v)
{
    value = v;
    next = NULL;
}

queue::queue()
{
    isempty = 1;
    length = 0;
    first = NULL;
    last = NULL;
}

queue::~queue()
{
    element *it = first;
    for(int i = 0; i < length; i++)
    {
        element *temp = it;
        it = it->next;
        delete temp;
    }
    first = NULL;
    last = NULL;
}

queue::queue(const queue &q)
{
    element *it = first;
    for(int i = 0; i < length; i++)
    {
        element *temp = it;
        if(it) it = it->next;
        delete temp;
    }
    first = NULL;
    last = NULL;
    length = 0;
    isempty = true;

    it = q.first;
    for(int i = 0; i < q.length; i++)
    {
        if (it)
        {
            Add_bottom(it->value);

            it = it->next;
            if(!it) break;
        }
    }
}

bool queue::IsEmpty()
{
    return isempty;
}

bool queue::Multiple(int v)
{
    int counter = 0;
    element *it = first;
    while(it)
    {
        if(it->value == v)
        {
            counter++;
        }
        it = it->next;
    }
    return (counter > 1);
}

// zwraca wskaznik na element o danym indeksie
element* queue:: Find_index(int v)
{
    if(isempty) return NULL;
    if(v < 0) return NULL;
    element *it = first;
    // zwraca wskaznik na element o indeksie v, ewentualnie ostatni
    while (it->next)
    {
        if(v == 0)
        {
            return it;
        }
        v--;
        it = it->next;
    }
    return it;

}

// zwraca wskaznik na element przed elementem o danej wartosci
element* queue:: Find_v_prev(int v)
{
    element *prev = first;
    if( !first ) return NULL;
    if( first->value == v ) return NULL;
    if( prev->next == NULL) return NULL;
    element *it = prev->next;
    while(it)
    {
        if(it->value == v)
        {
            return prev;
        }
        it = it->next;
        prev = prev->next;
    }
    return NULL;
}

// zwraca wskaznik na element o
element* queue:: Find_v_exact(int v)
{
    element *it = first;
    while(it)
    {
        if(it->value == v)
        {
            return it;
        }
        it = it->next;
    }
    return NULL;
}

// ummieszcza wartosc na koncu kolejki
void queue::Add_bottom(int v)
{
    element * el = new element(v);
    el->next = NULL;
    if (!last)
    {
        first = el;
        last = el;
        LengthAdd();
    }
    else
    {
        last->next = el;
        last = el;
        LengthAdd();
    }
}

// umieszcza wartosc na poczatku kolejki
void queue::Add_top(int v)
{
    element * el = new element(v);
    el->next = NULL;
    if(first)
    {
        el->next = first;
        first = el;
        LengthAdd();
    }
    else
    {
        first = el;
        last = el;
        LengthAdd();
    }
}

// umieszcza wartosc za dana wartoscia
void queue::Add_value(int v, int i)
{
    element * el = new element(v);
    el->next = NULL;
    element *target = Find_v_exact(i);
    if(target)
    {
        el->next = target->next;
        target->next = el;
        LengthAdd();
    }
}

// umieszcza wartosc na danym indeksie
void queue::Add_index(int v, int i)
{
    if(i > length || i < 0) return;
    element * el = new element(v);
    el->next = NULL;
    element * target = Find_index(i-1);
    if(!target)
    {
        Add_top(v);
    }
    else
    {
        el->next = target ->next;
        target->next = el;
        LengthAdd();
    }
}

int queue::GetValue_top()
{
    if(first) return first->value;
    return -1;
}

int queue::GetValue_bottom()
{
    if(last) return last->value;
    return -1;
}

int queue::GetValue_index(int i)
{
    element* el = Find_index(i);
    if (el)return el->value;
    return -1;
}

// usuwa z poczatku
void queue::Remove_top()
{
    if(isempty) return;
    if(first)
    {
        element *temp = first;
        first = first->next;
        delete temp;
        LengthSubdivide();
    }
}

// usuwa z konca
void queue::Remove_bottom()
{
    if(isempty) return;
    if(last)
    {
        element * prev = Find_index(length - 2);
        delete last;
        last = prev;
        LengthSubdivide();
    }
}

// usuwa z danego indeksu
void queue::Remove_index(int i)
{
    if(isempty) return;
    if(i < 0 || i > length - 1) return;
    if(i == 0)
    {
        Remove_top();
        return;
    }
    else if(i == length - 1)
    {
        Remove_bottom();
        return;
    }
    element *before = Find_index(i-1);
    if( before->next )
    {
        if(before->next->next)
        {
            element* after;
            after = before->next->next;
            element *temp = before->next;
            before->next = after;
            delete temp;
            LengthSubdivide();
        }
        else
        {
            element *temp = before->next;
            before->next = NULL;
            delete temp;
            LengthSubdivide();
        }
    }
}

// usuwa dana wartosc
void queue::Remove_value(int i)
{
    if(first && first->value == i)
    {
        Remove_top();
    }
    else
    {
        element *before = Find_v_prev(i);
        element *after;
        if(before)
        {
            if(before->next->next)
            {
                after = before->next->next;
                element *temp = before->next;
                before->next = after;
                delete temp;
                LengthSubdivide();
            }
            else
            {
                element *temp = before->next;
                before->next = NULL;
                delete temp;
                LengthSubdivide();
            }
        }
    }
}

// usuwa powtorki
void queue::RemoveDoubles()
{
    element *it = first;
    while (it)
    {
        int val = it->value;
        if( Multiple(val) )
        {
            it = it->next;
            Remove_value(val);
        }
        else
        {
            it = it->next;
        }
    }
}

//zwraca wartosc
int queue::GetLength()
{
    return length;
}


std::ostream& operator<<(std::ostream &os, const queue &q)
{
    element *it = q.first;
    int i = q.length;
    while(i--)
    {
        os<<it->value<<" ";
        it = it->next;
    }
    return os;
}

int queue:: operator[](int i) {return queue::GetValue_index(i);}

queue queue:: operator +( const queue &q2)
{
    queue result;
    int n = length;
    element *it = first;
    for (int i = 0; i < n; i++)
    {
        if(it)
        {
            result.Add_bottom(it->value);
            it = it->next;
        }
    }
    n = q2.length;
    it = q2.first;
    for (int i = 0; i < n; i++)
    {
        if(it)
        {
            result.Add_bottom(it->value);
            it = it->next;
        }
    }
    return result;
}

queue queue:: operator- ( const queue &q2)
{
    queue result;
    int n = length;
    element *it = first;
    for(int i = 0; i < n; i++)
    {
        if(it)
        {
            result.Add_bottom(it->value);
            it = it->next;
        }
    }

    it = q2.first;
    n = q2.length;
    for (int i = 0; i < n; i++)
    {
        result.Remove_value(it->value);
        it = it->next;
    }

    return result;
}

bool queue:: operator== ( const queue &q2)
{
    if(length != q2.length) return false;
    element *it1 = first;
    element *it2 = q2.first;
    while(it1 != NULL && it2 != NULL)
    {
        if(it1->value != it2->value)
        {
            return false;
        }

        it1 = it1->next;
        it2 = it2->next;
    }
    return true;
}

bool queue:: operator< ( const queue &q2)
{
    element *it1 = first;
    element *it2 = q2.first;
    while(it1 != NULL && it2 != NULL)
    {
        if(it1->value < it2->value)
        {
            return true;
        }
        else if (it1->value > it2->value)
        {
            return false;
        }

        it1 = it1->next;
        it2 = it2->next;
    }
    if(length < q2.length) return true;
    return false;
}

bool queue:: operator> ( const queue &q2)
{
    element *it1 = first;
    element *it2 = q2.first;
    while(it1 != NULL && it2 != NULL)
    {
        if(it1->value > it2->value)
        {
            return true;
        }
        else if (it1->value < it2->value)
        {
            return false;
        }

        it1 = it1->next;
        it2 = it2->next;
    }
    if(length > q2.length) return true;
    return false;
}

bool queue:: operator<= ( const queue &q2)
{
    return !(*this>q2);
}

bool queue:: operator>= ( const queue &q2)
{
    return !(*this<q2);
}

queue& queue::operator= (const queue &q)
{
    if(*this == q) return *this;

    element *it = first;
    for(int i = 0; i < length; i++)
    {
        element *temp = it;
        if(it) it = it->next;
        delete temp;
    }
    first = NULL;
    last = NULL;
    length = 0;
    isempty = true;

    it = q.first;
    for(int i = 0; i < q.length; i++)
    {
        if(it)
        {
            Add_bottom(it->value);
            it = it->next;
        }
    }
    return *this;
}

queue queue::operator+=(const queue &q)
{
    if(*this == q) return *this + q;

    element *it = first;
    it = q.first;
    for(int i = 0; i < q.length; i++)
    {
        if(it)
        {
            Add_bottom(it->value);
            it = it->next;
        }
    }
    return *this;
}

queue& queue::operator-=( const queue &q)
{
    if(*this == q)
    {
        *this = queue();
        return *this;
    }
    //result = *this;

    element *it = first;
    it = q.first;
    for(int i = 0; i < q.length; i++)
    {
        Remove_value(it->value);
        it = it->next;
    }
    //*this = result;
    return *this;
}

